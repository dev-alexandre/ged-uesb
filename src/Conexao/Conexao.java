/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Conexao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;



/**
 *
 * @author lucas
 */
public class Conexao {

    private static EntityManagerFactory emf = null;
    private static EntityManager em = null;

    private Conexao(){
       emf = Persistence.createEntityManagerFactory("gedPU");
       em = emf.createEntityManager();
    }

    public static EntityManager getEntityManager(){
        if(emf==null){
            new Conexao();
        }
        return em;
    }

}
