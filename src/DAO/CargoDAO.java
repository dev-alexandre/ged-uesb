/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Beans.Cargo;

/**
 *
 * @author lucas
 */
public class CargoDAO extends DAO <Cargo,Integer> implements InterfaceCargoDAO{
	
	public List<Cargo> buscarCargoPorDepartamento(Integer departamentoId) {
        
		String sql="select c from Cargo c where c.departamento.departamentoId=:departamentoId";
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("departamentoId", departamentoId);

        return listPesqParam(sql, params);
	}    

}
