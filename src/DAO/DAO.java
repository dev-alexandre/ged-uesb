/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import Conexao.Conexao;

public class DAO <T,ID extends Serializable> implements InterfaceDAOGenerico<T, ID>{

    private EntityManager entityManager;
    private final Class<T> oClass;
    private EntityTransaction tx;

    public Class<T> getObjectClass() {
        return this.oClass;
    }

    public void setEntityManager(EntityManager em){
        this.entityManager=em;
    }

    protected  EntityManager getEntityManager(){
        if(entityManager==null){
            throw new IllegalStateException("Erro");
        }
        return entityManager;
    }

    public DAO(){
        this.oClass=(Class<T>)((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        setEntityManager(Conexao.getEntityManager());
    }
    
    public void beginTransation() {
    	tx = getEntityManager().getTransaction();
    	tx.begin();
    }
    
    public void commitTransation() {
    	if(tx.isActive()) {
    		tx.commit();
    	}
    	getEntityManager().clear();
	}
	
	public void rollbackTransation() {
		if(tx.isActive()) {
			tx.rollback();
		}
		getEntityManager().clear();
	}


    @Override
    public T salvar(T obj) {
        getEntityManager().persist(obj);
        getEntityManager().flush();
        return obj;
    }

	

    @Override
    public T atualizar(T obj) {
        getEntityManager().merge(obj);
        getEntityManager().flush();
        return obj;
    }

    @Override
    public void excluir(T obj) {
        obj=getEntityManager().merge(obj);
        getEntityManager().remove(obj);
        getEntityManager().flush();
    }

    @Override
    public T pesquisarPorId(ID id) {
    	getEntityManager().clear();
        return (T) getEntityManager().find(oClass, id);
    }

    public List<T> todos() {
        String query = "SELECT obj FROM "+ oClass.getSimpleName() +" obj";
        Query q = getEntityManager().createQuery(query);
        return q.getResultList();
    }
    
    public List<T> todos(int atual, int max) {
        String query = "SELECT obj FROM "+ oClass.getSimpleName() +" obj";
        Query q = getEntityManager().createQuery(query).setMaxResults(max).setFirstResult(atual);
        return q.getResultList();
    }

    public List<T> listPesqParam(String query, Map<String, Object> params) {
        Query q = getEntityManager().createQuery(query);
        for(String chave : params.keySet()){
            q.setParameter(chave, params.get(chave));
        }

        return q.getResultList();
    }

    public List<T> listPesqParam(String query, Map<String, Object> params, int max, int atual) {
        Query q = getEntityManager().createQuery(query).setMaxResults(max).setFirstResult(atual);
        for(String chave : params.keySet()){
            q.setParameter(chave, params.get(chave));
        }

        return q.getResultList();
    }

    public List<T> listPesq(String query) {
        Query q = getEntityManager().createQuery(query);
        return q.getResultList();
    }

    public T pesqParam(String query, Map<String, Object> params) {
    	getEntityManager().clear();
        Query q = getEntityManager().createQuery(query);
        for(String chave : params.keySet()){
            q.setParameter(chave, params.get(chave));
        }
        try{
            return (T) q.getSingleResult();
        }
        catch(NoResultException erro){ 
            return null;
        }
    }

}
