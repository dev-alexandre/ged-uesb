/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import Beans.Usuario;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author lucas
 */
public class UsuarioDAO extends DAO <Usuario,Integer> implements InterfaceUsuarioDAO{
    
    public Usuario validaUsuario(Usuario usuario){

        Map<String,Object> params = new HashMap<String, Object>();
        params.put("login",usuario.getUsuarioLogin().toString());
        params.put("senha",usuario.getUsuarioSenha().toString());
        return (Usuario) pesqParam("select u "
        						 + "from Usuario u "
        						 + "inner join fetch u.funcionario func "
        						 + "where u.usuarioLogin=:login and u.usuarioSenha=:senha", params);        
        
    }

}
