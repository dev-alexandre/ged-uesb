/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import Beans.Cargo;
import Beans.Departamento;
import Beans.Encaminhamento;
import Beans.Funcionario;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import Util.EnumStatusEncaminhamento;

/**
 *
 * @author lucas
 */
public class EncaminhamentoDAO extends DAO<Encaminhamento, Integer> implements InterfaceEncaminhamentoDAO {

    public List<Encaminhamento> encaminhamentosRecebidosPorDepartamento(Departamento departamento) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
        		+ "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.departamentoDestino=:departamento "
                ;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("departamento", departamento);

        return listPesqParam(sql, params);
    }
    
    public List<Encaminhamento> encaminhamentosRecebidosPorDepartamentoStatus(Departamento departamento) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
        		+ "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.departamentoDestino=:departamento "
                + "and e.encaminhamentoStatus<>:status";
                ;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("departamento", departamento);
        params.put("status", EnumStatusEncaminhamento.FINALIZADO.getStatus());

        return listPesqParam(sql, params);
    }
    
    public List<Encaminhamento> encaminhamentosRecebidosPorDepartamentoDocumentoDescricao(Departamento departamento, String descricao) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
                + "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.departamentoDestino=:departamento "
                + "and e.documento.documentoDescricao like :descricao";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("departamento", departamento);
        params.put("descricao", descricao+"%");

        return listPesqParam(sql, params);
    }
    
    

    public List<Encaminhamento> encaminhamentosRecebidosPorCargoDepartamento(Departamento departamento, Cargo cargo) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
                + "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.departamento=:departamento "
                + "and e.departamentoDestino.cargo=:cargo";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("departamento", departamento);
        params.put("cargo", cargo);

        return listPesqParam(sql, params);

    }
    
    public List<Encaminhamento> encaminhamentosFeitosPorFuncionario(Funcionario funcionario) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
                + "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.documento.funcionario.funcionarioMatricula=:funcionario "
                
                ;

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("funcionario", funcionario.getFuncionarioMatricula());

        return listPesqParam(sql, params);

    }
    
    public List<Encaminhamento> encaminhamentosFeitosPorFuncionarioStatus(Funcionario funcionario) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
                + "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.documento.funcionario.funcionarioMatricula=:funcionario "
                + "and e.encaminhamentoStatus<>:status";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("funcionario", funcionario.getFuncionarioMatricula());
        params.put("status", EnumStatusEncaminhamento.FINALIZADO.getStatus());

        return listPesqParam(sql, params);

    }
    
    public List<Encaminhamento> encaminhamentosFeitosPorFuncionarioDescricao(Funcionario funcionario, String descricao) {

        String sql = "select distinct e "
                + "from Encaminhamento e "
                + "inner join fetch e.documento "
                + "inner join fetch e.assentamentoList "
                + "where e.documento.funcionario.funcionarioMatricula=:funcionario "
                + "and e.documento.documentoDescricao like :descricao";

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("funcionario", funcionario.getFuncionarioMatricula());
        params.put("descricao", descricao+"%");

        return listPesqParam(sql, params);

    }
}
