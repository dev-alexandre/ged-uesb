/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import Beans.Departamento;
import Beans.Documento;
import Beans.Funcionario;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author lucas
 */
public class DocumentoDAO extends DAO <Documento,Integer> implements InterfaceDocumentoDAO{
    
    public List<Documento> getTodosPorFuncionario(Funcionario funcionario){
        
        String sql="select documento "
                 + "from Documento documento "
                 + "where documento.funcionario=:funcionario";
        
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("funcionario", funcionario);
        
        return listPesqParam(sql, params);
    }
    
    public List<Documento> getTodosPorFuncionarioDescricao(Funcionario funcionario, String descricao){
        
        String sql="select documento "
                 + "from Documento documento "
                 + "where documento.funcionario=:funcionario "
                 + "and documento.documentoDescricao like :descricao ";
        
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("funcionario", funcionario);
        params.put("descricao", descricao+"%");
        
        return listPesqParam(sql, params);
    }
    
    public List<Documento> getTodosRecebidosPorDepartamento(Departamento departamento){
        
        String sql = "select e.documento "
                   + "from Encaminhamento e "
                   + "where e.departamentoDestino=:departamento";
        
        Map<String,Object> params = new HashMap<String, Object>();
        params.put("departamento", departamento);
        
        return listPesqParam(sql, params);
    }

}
