/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;

import java.io.Serializable;
import java.util.List;
import java.util.Map;



public interface InterfaceDAOGenerico <T, ID extends Serializable>  {


    public Class <T> getObjectClass();
    public T salvar(T obj);
    public T pesquisarPorId(ID id);
    public T atualizar(T obj);
    public void excluir(T obj);
    public List<T> todos();
    public List<T> listPesqParam(String query, Map<String,Object> params);
    public List<T> listPesqParam(String query, Map<String,Object> params, int max, int atual);
    public List<T> listPesq(String query);
    public T pesqParam(String query, Map<String,Object> params);

}