/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "encaminhamento")
@NamedQueries({
    @NamedQuery(name = "Encaminhamento.findAll", query = "SELECT e FROM Encaminhamento e")})
public class Encaminhamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "encaminhamento_id")
    private Integer encaminhamentoId;
    @Basic(optional = false)
    @Column(name = "encaminhamento_data")
    @Temporal(TemporalType.DATE)
    private Date encaminhamentoData;
    @Basic(optional = false)
    @Column(name = "encaminhamento_status")
    private String encaminhamentoStatus;
    @OneToMany(mappedBy = "encaminhamento")
    private List<Assentamento> assentamentoList;
    @JoinColumn(name = "encaminhamento_documento_id", referencedColumnName = "documento_id")
    @ManyToOne
    private Documento documento;
    @JoinColumn(name = "encaminhamento_departamento_destino", referencedColumnName = "departamento_id")
    @ManyToOne
    private Departamento departamentoDestino;
    @JoinColumn(name = "encaminhamento_departamento_remetente", referencedColumnName = "departamento_id")
    @ManyToOne
    private Departamento departamentoRemetente;

    public Encaminhamento() {
    }

    public Encaminhamento(Integer encaminhamentoId) {
        this.encaminhamentoId = encaminhamentoId;
    }

    public Encaminhamento(Integer encaminhamentoId, Date encaminhamentoData, String encaminhamentoStatus) {
        this.encaminhamentoId = encaminhamentoId;
        this.encaminhamentoData = encaminhamentoData;
        this.encaminhamentoStatus = encaminhamentoStatus;
    }

    public Integer getEncaminhamentoId() {
        return encaminhamentoId;
    }

    public void setEncaminhamentoId(Integer encaminhamentoId) {
        this.encaminhamentoId = encaminhamentoId;
    }

    public Date getEncaminhamentoData() {
        return encaminhamentoData;
    }

    public void setEncaminhamentoData(Date encaminhamentoData) {
        this.encaminhamentoData = encaminhamentoData;
    }

    public String getEncaminhamentoStatus() {
        return encaminhamentoStatus;
    }

    public void setEncaminhamentoStatus(String encaminhamentoStatus) {
        this.encaminhamentoStatus = encaminhamentoStatus;
    }

    public List<Assentamento> getAssentamentoList() {
        return assentamentoList;
    }

    public void setAssentamentoList(List<Assentamento> assentamentoList) {
        this.assentamentoList = assentamentoList;
    }

    public Departamento getDepartamentoDestino() {
        return departamentoDestino;
    }

    public void setDepartamentoDestino(Departamento departamentoDestino) {
        this.departamentoDestino = departamentoDestino;
    }

    public Departamento getDepartamentoRemetente() {
        return departamentoRemetente;
    }

    public void setDepartamentoRemetente(Departamento departamentoRemetente) {
        this.departamentoRemetente = departamentoRemetente;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((encaminhamentoId == null) ? 0 : encaminhamentoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Encaminhamento other = (Encaminhamento) obj;
		if (encaminhamentoId == null) {
			if (other.encaminhamentoId != null)
				return false;
		} else if (!encaminhamentoId.equals(other.encaminhamentoId))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return String.valueOf(encaminhamentoId);
    }
    
}
