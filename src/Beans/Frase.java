/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "frase")
@NamedQueries({
    @NamedQuery(name = "Frase.findAll", query = "SELECT f FROM Frase f")})
public class Frase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "frase_id")
    private Integer fraseId;
    @Basic(optional = false)
    @Column(name = "frase_autor")
    private String fraseAutor;
    @Basic(optional = false)
    @Column(name = "frase_conteudo")
    private String fraseConteudo;

    public Frase() {
    }

    public Frase(Integer fraseId) {
        this.fraseId = fraseId;
    }

    public Frase(Integer fraseId, String fraseAutor, String fraseConteudo) {
        this.fraseId = fraseId;
        this.fraseAutor = fraseAutor;
        this.fraseConteudo = fraseConteudo;
    }

    public Integer getFraseId() {
        return fraseId;
    }

    public void setFraseId(Integer fraseId) {
        this.fraseId = fraseId;
    }

    public String getFraseAutor() {
        return fraseAutor;
    }

    public void setFraseAutor(String fraseAutor) {
        this.fraseAutor = fraseAutor;
    }

    public String getFraseConteudo() {
        return fraseConteudo;
    }

    public void setFraseConteudo(String fraseConteudo) {
        this.fraseConteudo = fraseConteudo;
    }

    @Override
    public String toString() {
        return String.valueOf(fraseId);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fraseId == null) ? 0 : fraseId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Frase other = (Frase) obj;
		if (fraseId == null) {
			if (other.fraseId != null)
				return false;
		} else if (!fraseId.equals(other.fraseId))
			return false;
		return true;
	}
    
}
