/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "sugestao")
@NamedQueries({
    @NamedQuery(name = "Sugestao.findAll", query = "SELECT s FROM Sugestao s")})
public class Sugestao implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "sugestao_id")
    private Integer sugestaoId;
    @Basic(optional = false)
    @Column(name = "sugestao_data")
    @Temporal(TemporalType.DATE)
    private Date sugestaoData;
    @Basic(optional = false)
    @Column(name = "sugestao_conteudo")
    private String sugestaoConteudo;

    public Sugestao() {
    }

    public Sugestao(Integer sugestaoId) {
        this.sugestaoId = sugestaoId;
    }

    public Sugestao(Integer sugestaoId, Date sugestaoData, String sugestaoConteudo) {
        this.sugestaoId = sugestaoId;
        this.sugestaoData = sugestaoData;
        this.sugestaoConteudo = sugestaoConteudo;
    }

    public Integer getSugestaoId() {
        return sugestaoId;
    }

    public void setSugestaoId(Integer sugestaoId) {
        this.sugestaoId = sugestaoId;
    }

    public Date getSugestaoData() {
        return sugestaoData;
    }

    public void setSugestaoData(Date sugestaoData) {
        this.sugestaoData = sugestaoData;
    }

    public String getSugestaoConteudo() {
        return sugestaoConteudo;
    }

    public void setSugestaoConteudo(String sugestaoConteudo) {
        this.sugestaoConteudo = sugestaoConteudo;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((sugestaoId == null) ? 0 : sugestaoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Sugestao other = (Sugestao) obj;
		if (sugestaoId == null) {
			if (other.sugestaoId != null)
				return false;
		} else if (!sugestaoId.equals(other.sugestaoId))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return String.valueOf(sugestaoId);
    }
    
}
