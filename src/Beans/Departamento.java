/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "departamento")
@NamedQueries({
    @NamedQuery(name = "Departamento.findAll", query = "SELECT d FROM Departamento d")})
public class Departamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "departamento_id")
    private Integer departamentoId;
    @Basic(optional = false)
    @Column(name = "departamento_nome")
    private String departamentoNome;
    @Basic(optional = false)
    @Column(name = "departamento_sigla")
    private String departamentoSigla;
    @OneToMany(mappedBy = "departamento")
    private List<Documento> documentoList;
    @OneToMany(mappedBy = "departamento")
    private List<Funcionario> funcionarioList;
    @OneToMany(mappedBy = "departamentoDestino")
    private List<Encaminhamento> encaminhamentoList;
    @OneToMany(mappedBy = "departamentoRemetente")
    private List<Encaminhamento> encaminhamentoList1;
    @OneToMany(mappedBy = "departamento")
    private List<Cargo> cargoList;

    public Departamento() {
    }

    public Departamento(Integer departamentoId) {
        this.departamentoId = departamentoId;
    }

    public Departamento(Integer departamentoId, String departamentoNome, String departamentoSigla) {
        this.departamentoId = departamentoId;
        this.departamentoNome = departamentoNome;
        this.departamentoSigla = departamentoSigla;
    }

    public Integer getDepartamentoId() {
        return departamentoId;
    }

    public void setDepartamentoId(Integer departamentoId) {
        this.departamentoId = departamentoId;
    }

    public String getDepartamentoNome() {
        return departamentoNome;
    }

    public void setDepartamentoNome(String departamentoNome) {
        this.departamentoNome = departamentoNome;
    }

    public String getDepartamentoSigla() {
        return departamentoSigla;
    }

    public void setDepartamentoSigla(String departamentoSigla) {
        this.departamentoSigla = departamentoSigla;
    }

    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    public List<Funcionario> getFuncionarioList() {
        return funcionarioList;
    }

    public void setFuncionarioList(List<Funcionario> funcionarioList) {
        this.funcionarioList = funcionarioList;
    }

    public List<Encaminhamento> getEncaminhamentoList() {
        return encaminhamentoList;
    }

    public void setEncaminhamentoList(List<Encaminhamento> encaminhamentoList) {
        this.encaminhamentoList = encaminhamentoList;
    }

    public List<Encaminhamento> getEncaminhamentoList1() {
        return encaminhamentoList1;
    }

    public void setEncaminhamentoList1(List<Encaminhamento> encaminhamentoList1) {
        this.encaminhamentoList1 = encaminhamentoList1;
    }

    public List<Cargo> getCargoList() {
        return cargoList;
    }

    public void setCargoList(List<Cargo> cargoList) {
        this.cargoList = cargoList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((departamentoId == null) ? 0 : departamentoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Departamento other = (Departamento) obj;
		if (departamentoId == null) {
			if (other.departamentoId != null)
				return false;
		} else if (!departamentoId.equals(other.departamentoId))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return String.valueOf(departamentoId);
    }
    
}
