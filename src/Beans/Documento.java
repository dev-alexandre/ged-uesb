/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "documento")
@NamedQueries({
    @NamedQuery(name = "Documento.findAll", query = "SELECT d FROM Documento d")})
public class Documento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "documento_id")
    private Integer documentoId;
    @Basic(optional = false)
    @Column(name = "documento_tipo")
    private String documentoTipo;
    @Basic(optional = false)
    @Column(name = "documento_data_criacao")
    @Temporal(TemporalType.DATE)
    private Date documentoDataCriacao;
    @Basic(optional = false)
    @Column(name = "documento_descricao")
    private String documentoDescricao;
    @Basic(optional = false)
    @Column(name = "documento_resumo")
    private String documentoResumo;
    @Basic(optional = false)
    @Column(name = "documento_caminho_arquivo")
    private String documentoCaminhoArquivo;
    @JoinColumn(name = "documento_funcionario_matricula", referencedColumnName = "funcionario_matricula")
    @ManyToOne
    private Funcionario funcionario;
    @JoinColumn(name = "documento_departamento_id", referencedColumnName = "departamento_id")
    @ManyToOne
    private Departamento departamento;
    @OneToMany(mappedBy = "documento")
    private List<Encaminhamento> encaminhamentoList;

    public Documento() {
    }

    public Documento(Integer documentoId) {
        this.documentoId = documentoId;
    }

    public Documento(Integer documentoId, String documentoTipo, Date documentoDataCriacao, String documentoDescricao, String documentoResumo, String documentoCaminhoArquivo) {
        this.documentoId = documentoId;
        this.documentoTipo = documentoTipo;
        this.documentoDataCriacao = documentoDataCriacao;
        this.documentoDescricao = documentoDescricao;
        this.documentoResumo = documentoResumo;
        this.documentoCaminhoArquivo = documentoCaminhoArquivo;
    }

    public Integer getDocumentoId() {
        return documentoId;
    }

    public void setDocumentoId(Integer documentoId) {
        this.documentoId = documentoId;
    }

    public String getDocumentoTipo() {
        return documentoTipo;
    }

    public void setDocumentoTipo(String documentoTipo) {
        this.documentoTipo = documentoTipo;
    }

    public Date getDocumentoDataCriacao() {
        return documentoDataCriacao;
    }

    public void setDocumentoDataCriacao(Date documentoDataCriacao) {
        this.documentoDataCriacao = documentoDataCriacao;
    }

    public String getDocumentoDescricao() {
        return documentoDescricao;
    }

    public void setDocumentoDescricao(String documentoDescricao) {
        this.documentoDescricao = documentoDescricao;
    }

    public String getDocumentoResumo() {
        return documentoResumo;
    }

    public void setDocumentoResumo(String documentoResumo) {
        this.documentoResumo = documentoResumo;
    }

    public String getDocumentoCaminhoArquivo() {
        return documentoCaminhoArquivo;
    }

    public void setDocumentoCaminhoArquivo(String documentoCaminhoArquivo) {
        this.documentoCaminhoArquivo = documentoCaminhoArquivo;
    }

    public List<Encaminhamento> getEncaminhamentoList() {
        return encaminhamentoList;
    }

    public void setEncaminhamentoList(List<Encaminhamento> encaminhamentoList) {
        this.encaminhamentoList = encaminhamentoList;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }
    
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((documentoId == null) ? 0 : documentoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Documento other = (Documento) obj;
		if (documentoId == null) {
			if (other.documentoId != null)
				return false;
		} else if (!documentoId.equals(other.documentoId))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return String.valueOf(documentoId);
    }
    
}
