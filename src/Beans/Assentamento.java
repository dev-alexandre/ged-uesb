/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "assentamento")
@NamedQueries({
    @NamedQuery(name = "Assentamento.findAll", query = "SELECT a FROM Assentamento a")})
public class Assentamento implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "assentamento_id")
    private Integer assentamentoId;
    @Basic(optional = false)
    @Column(name = "assentamento_conteudo")
    private String assentamentoConteudo;
    @JoinColumn(name = "assentamento_funcionario_matricula", referencedColumnName = "funcionario_matricula")
    @ManyToOne
    private Funcionario funcionario;
    @JoinColumn(name = "assentamento_encaminhamento_id", referencedColumnName = "encaminhamento_id")
    @ManyToOne
    private Encaminhamento encaminhamento;

    public Assentamento() {
    }

    public Assentamento(Integer assentamentoId) {
        this.assentamentoId = assentamentoId;
    }

    public Assentamento(Integer assentamentoId, String assentamentoConteudo) {
        this.assentamentoId = assentamentoId;
        this.assentamentoConteudo = assentamentoConteudo;
    }

    public Integer getAssentamentoId() {
        return assentamentoId;
    }

    public void setAssentamentoId(Integer assentamentoId) {
        this.assentamentoId = assentamentoId;
    }

    public String getAssentamentoConteudo() {
        return assentamentoConteudo;
    }

    public void setAssentamentoConteudo(String assentamentoConteudo) {
        this.assentamentoConteudo = assentamentoConteudo;
    }

    public Encaminhamento getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(Encaminhamento encaminhamento) {
        this.encaminhamento = encaminhamento;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assentamentoId == null) ? 0 : assentamentoId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Assentamento other = (Assentamento) obj;
		if (assentamentoId == null) {
			if (other.assentamentoId != null)
				return false;
		} else if (!assentamentoId.equals(other.assentamentoId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.valueOf(assentamentoId);
	}
    
}
