/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;
import java.util.List;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "funcionario")
@NamedQueries({
    @NamedQuery(name = "Funcionario.findAll", query = "SELECT f FROM Funcionario f")})
public class Funcionario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "funcionario_matricula")
    private String funcionarioMatricula;
    @Basic(optional = false)
    @Column(name = "funcionario_nome")
    private String funcionarioNome;
    @Basic(optional = false)
    @Column(name = "funcionario_email")
    private String funcionarioEmail;
    @OneToMany(mappedBy = "funcionario")
    private List<Assentamento> assentamentoList;
    @OneToMany(mappedBy = "funcionario")
    private List<Usuario> usuarioList;
    @OneToMany(mappedBy = "funcionario")
    private List<Documento> documentoList;
    @JoinColumn(name = "funcionario_departamento_id", referencedColumnName = "departamento_id")
    @ManyToOne
    private Departamento departamento;
    @JoinColumn(name = "funcionario_cargo_id", referencedColumnName = "cargo_id")
    @ManyToOne
    private Cargo cargo;

    public Funcionario() {
    }

    public Funcionario(String funcionarioMatricula) {
        this.funcionarioMatricula = funcionarioMatricula;
    }

    public Funcionario(String funcionarioMatricula, String funcionarioNome, String funcionarioEmail) {
        this.funcionarioMatricula = funcionarioMatricula;
        this.funcionarioNome = funcionarioNome;
        this.funcionarioEmail = funcionarioEmail;
    }

    public String getFuncionarioMatricula() {
        return funcionarioMatricula;
    }

    public void setFuncionarioMatricula(String funcionarioMatricula) {
        this.funcionarioMatricula = funcionarioMatricula;
    }

    public String getFuncionarioNome() {
        return funcionarioNome;
    }

    public void setFuncionarioNome(String funcionarioNome) {
        this.funcionarioNome = funcionarioNome;
    }

    public String getFuncionarioEmail() {
        return funcionarioEmail;
    }

    public void setFuncionarioEmail(String funcionarioEmail) {
        this.funcionarioEmail = funcionarioEmail;
    }

    public List<Assentamento> getAssentamentoList() {
        return assentamentoList;
    }

    public void setAssentamentoList(List<Assentamento> assentamentoList) {
        this.assentamentoList = assentamentoList;
    }

    public List<Usuario> getUsuarioList() {
        return usuarioList;
    }

    public void setUsuarioList(List<Usuario> usuarioList) {
        this.usuarioList = usuarioList;
    }

    public List<Documento> getDocumentoList() {
        return documentoList;
    }

    public void setDocumentoList(List<Documento> documentoList) {
        this.documentoList = documentoList;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((funcionarioMatricula == null) ? 0 : funcionarioMatricula
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Funcionario other = (Funcionario) obj;
		if (funcionarioMatricula == null) {
			if (other.funcionarioMatricula != null)
				return false;
		} else if (!funcionarioMatricula.equals(other.funcionarioMatricula))
			return false;
		return true;
	}

	@Override
    public String toString() {
        return funcionarioMatricula;
    }
    
}
