/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.io.Serializable;

import javax.persistence.*;

/**
 *
 * @author lucas
 */
@Entity
@Table(name = "usuario")
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")})
public class Usuario implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "usuario_id")
    private Integer usuarioId;
    @Basic(optional = false)
    @Column(name = "usuario_login")
    private String usuarioLogin;
    @Basic(optional = false)
    @Column(name = "usuario_senha")
    private String usuarioSenha;
    @Basic(optional = false)
    @Column(name = "usuario_status")
    private boolean usuarioStatus;
    @Basic(optional = false)
    @Column(name = "usuario_privilegio")
    private String usuarioPrivilegio;
    @JoinColumn(name = "usuario_funcionario_matricula", referencedColumnName = "funcionario_matricula")
    @ManyToOne
    private Funcionario funcionario;

    public Usuario() {
    }

    public Usuario(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Usuario(Integer usuarioId, String usuarioLogin, String usuarioSenha, boolean usuarioStatus, String usuarioPrivilegio) {
        this.usuarioId = usuarioId;
        this.usuarioLogin = usuarioLogin;
        this.usuarioSenha = usuarioSenha;
        this.usuarioStatus = usuarioStatus;
        this.usuarioPrivilegio = usuarioPrivilegio;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getUsuarioLogin() {
        return usuarioLogin;
    }

    public void setUsuarioLogin(String usuarioLogin) {
        this.usuarioLogin = usuarioLogin;
    }

    public String getUsuarioSenha() {
        return usuarioSenha;
    }

    public void setUsuarioSenha(String usuarioSenha) {
        this.usuarioSenha = usuarioSenha;
    }

    public boolean getUsuarioStatus() {
        return usuarioStatus;
    }

    public void setUsuarioStatus(boolean usuarioStatus) {
        this.usuarioStatus = usuarioStatus;
    }

    public String getUsuarioPrivilegio() {
        return usuarioPrivilegio;
    }

    public void setUsuarioPrivilegio(String usuarioPrivilegio) {
        this.usuarioPrivilegio = usuarioPrivilegio;
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    @Override
    public String toString() {
        return String.valueOf(usuarioId);
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((usuarioId == null) ? 0 : usuarioId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (usuarioId == null) {
			if (other.usuarioId != null)
				return false;
		} else if (!usuarioId.equals(other.usuarioId))
			return false;
		return true;
	}
    
}
