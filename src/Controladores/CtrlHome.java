/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

import Beans.Encaminhamento;
import Beans.Usuario;
import DAO.EncaminhamentoDAO;
import Util.Sessao;


/**
 *
 * @author lucas
 */

@ManagedBean
@RequestScoped
public class CtrlHome implements Serializable {
    
	private static final long serialVersionUID = 4055560164485369990L;
	private CtrlLogin login;
    private Usuario usuario;
    private List<Encaminhamento> encaminhamentosFeitos;
    private List<Encaminhamento> encaminhamentosRecebidosDepartamento;
    private Encaminhamento encaminhamentoSelecionado;
    private EncaminhamentoDAO encaminhamentoDAO;
    

    public CtrlHome() {
    	encaminhamentoDAO = new EncaminhamentoDAO();
        recuperarUsuario();
        carregaEncaminhamentosFeitos();
        carregaEncaminhamentosRecebidosDepartamento();
        
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public List<Encaminhamento> getEncaminhamentosFeitos() {
        return encaminhamentosFeitos;
    }

    public void setEncaminhamentosFeitos(List<Encaminhamento> encaminhamentosFeitos) {
        this.encaminhamentosFeitos = encaminhamentosFeitos;
    }

    public List<Encaminhamento> getEncaminhamentosRecebidosDepartamento() {
        return encaminhamentosRecebidosDepartamento;
    }

    public void setEncaminhamentosRecebidosDepartamento(List<Encaminhamento> encaminhamentosRecebidosDepartamento) {
        this.encaminhamentosRecebidosDepartamento = encaminhamentosRecebidosDepartamento;
    }

    public Encaminhamento getEncaminhamentoSelecionado() {
        return encaminhamentoSelecionado;
    }

    public void setEncaminhamentoSelecionado(Encaminhamento encaminhamentoSelecionado) {
        this.encaminhamentoSelecionado = encaminhamentoSelecionado;
    }
    
    
    
    
    
    public String logout(){
        
        login.logout();
        return "index";
        
    }
    
    private void recuperarUsuario(){
        
        login = (CtrlLogin) Sessao.recuperarManageBean("#{ctrlLogin}", CtrlLogin.class);
        usuario = login.getUsuario();
        
    }
    
    public void carregaEncaminhamentosRecebidosDepartamento(){
        
        encaminhamentosRecebidosDepartamento=encaminhamentoDAO.encaminhamentosRecebidosPorDepartamentoStatus(usuario.getFuncionario().getDepartamento());
    }
    
    public void carregaEncaminhamentosFeitos(){
        
        encaminhamentosFeitos=encaminhamentoDAO.encaminhamentosFeitosPorFuncionarioStatus(usuario.getFuncionario());
    }
    
    public void visualizarEncaminhamento(){
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
        	Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
            fc.getExternalContext().redirect("visualizar_documento_encaminhado.jsf");
        } catch (IOException ex) {
            Logger.getLogger(CtrlHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void visualizarRecebimento(){
        FacesContext fc = FacesContext.getCurrentInstance();
        try {
        	Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
            fc.getExternalContext().redirect("visualizar_documento_recebido.jsf");
        } catch (IOException ex) {
            Logger.getLogger(CtrlHome.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
