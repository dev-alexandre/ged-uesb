/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import Beans.Documento;
import DAO.DocumentoDAO;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlVisualizarDocumento implements Serializable {

	private static final long serialVersionUID = 4543874935419555416L;
	
	private DocumentoDAO documentoDAO;

	private Documento documentoSelecionado;
	private boolean bloqueio;

	public CtrlVisualizarDocumento() {
		documentoDAO = new DocumentoDAO();
	}

	@PostConstruct
	public void load() {
		recuperaSessao();
		bloqueio = true;
	}
	
	public void redirectToHome() {
		if(documentoSelecionado == null) {
    		try {
    			FacesContext.getCurrentInstance().getExternalContext().redirect("home.jsf");
			} 
    		catch (IOException e) {
				e.printStackTrace();
			}
    	}
	}

	public String salvar() {

		try {
			documentoDAO.beginTransation();
			if(documentoSelecionado.getDocumentoId()==null) {
				documentoDAO.salvar(documentoSelecionado);
			}
			else {
				documentoDAO.atualizar(documentoSelecionado);
			}
			documentoDAO.commitTransation();
			Mensagens.msgInfo("O documento '"
					+ documentoSelecionado.getDocumentoDescricao()
					+ "' foi altualizado com sucesso!");
			documentoSelecionado = new Documento();
		} 
		catch (Exception e) {
			e.printStackTrace();
			documentoDAO.rollbackTransation();
			Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente novamente.");
		}
		informarPaginaRedirecionamento();
		return "mensagens";
	}
	
	private void informarPaginaRedirecionamento() {
		Mensagens.informarPaginaRedirecionamento("visualizar_documento");
	}

	private void recuperaSessao() {
		documentoSelecionado = (Documento) Sessao
				.recuperarRemoverObjetoSessao("documentoSelecionado");
	}

	public Documento getDocumentoSelecionado() {
		return documentoSelecionado;
	}

	public void setDocumentoSelecionado(Documento documentoSelecionado) {
		this.documentoSelecionado = documentoSelecionado;
	}

	public boolean getBloqueio() {
		return bloqueio;
	}

	public void setBloqueio(boolean bloqueio) {
		this.bloqueio = bloqueio;
	}
}