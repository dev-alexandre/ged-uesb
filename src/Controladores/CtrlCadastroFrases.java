/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Beans.Frase;
import DAO.FraseDAO;
import Util.Mensagens;


/**
 *
 * @author lucas
 */

@ManagedBean
@ViewScoped
public class CtrlCadastroFrases implements Serializable {

	private static final long serialVersionUID = -873425242736353762L;
	private Frase frase;
    private FraseDAO fraseDAO;

    public CtrlCadastroFrases() {

        frase = new Frase();
        fraseDAO = new FraseDAO();

    }

    public Frase getFrase() {
        return frase;
    }

    public void setFrase(Frase frase) {
        this.frase = frase;
    }

    public String salvar(){
        try{
        	fraseDAO.beginTransation();
            fraseDAO.salvar(frase);
            fraseDAO.commitTransation();
            Mensagens.msgInfo("Sua frase foi salva com sucesso!");
            frase = new Frase();
        }
        catch(Exception e){
        	fraseDAO.rollbackTransation();
        	e.printStackTrace();
            Mensagens.msgErro("Não foi possível realiza a sua solicitação. Por favor, tente novamente.");           

        }
        Mensagens.informarPaginaRedirecionamento("cadastro_frases");
        return "mensagens";
    }


}
