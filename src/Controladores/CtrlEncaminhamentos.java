/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import Beans.Assentamento;
import Beans.Departamento;
import Beans.Documento;
import Beans.Encaminhamento;
import Beans.Usuario;
import DAO.AssentamentoDAO;
import DAO.DepartamentoDAO;
import DAO.DocumentoDAO;
import DAO.EncaminhamentoDAO;
import Util.EnumStatusEncaminhamento;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlEncaminhamentos implements Serializable {

	private static final long serialVersionUID = 7516771629936486330L;
	private List<Documento> documentos;
    private Documento documentoSelecionado;
    private Encaminhamento encaminhamento;
    private Assentamento assentamento;
    
    private DocumentoDAO documentoDAO;
    private EncaminhamentoDAO encaminhamentoDAO;
    
    private Usuario usuario;
    private String consulta;
    
    public CtrlEncaminhamentos() {
        
        documentoDAO=new DocumentoDAO();
        encaminhamentoDAO=new EncaminhamentoDAO();
        zerarObjetos();
        recuperaSessao();
        carregaDocumentosDoFuncionario();
        
    }

    public Assentamento getAssentamento() {
        return assentamento;
    }

    public void setAssentamento(Assentamento assentamento) {
        this.assentamento = assentamento;
    }

    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }

    public Documento getDocumentoSelecionado() {
        return documentoSelecionado;
    }

    public void setDocumentoSelecionado(Documento documentoSelecionado) {
        this.documentoSelecionado = documentoSelecionado;
    }

    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }

    public Encaminhamento getEncaminhamento() {
        return encaminhamento;
    }

    public void setEncaminhamento(Encaminhamento encaminhamento) {
        this.encaminhamento = encaminhamento;
    }
    
    private void zerarObjetos(){
        documentoSelecionado=new Documento();
        encaminhamento=new Encaminhamento();
        encaminhamento.setEncaminhamentoData(new Date());
        assentamento=new Assentamento();
    }
    
    public List<SelectItem> getDepartamentos(){

        DepartamentoDAO departamentoDAO = new DepartamentoDAO();
        List<SelectItem> departmentos = new ArrayList<SelectItem>();

        for(Departamento d : departamentoDAO.todos()){
            departmentos.add(new SelectItem(d, d.getDepartamentoNome()+" - ("+d.getDepartamentoSigla()+")"));
        }

        return departmentos;

    }
    
    private void recuperaSessao(){
        usuario = (Usuario) Sessao.recuperarObjetoSessao("usuario");
    }
    
    public void carregaDocumentosDoFuncionario(){
        documentos=documentoDAO.getTodosPorFuncionario(usuario.getFuncionario());
    }
    
    public void carregaDocumentosDoFuncionarioPorDescricao(){
        documentos=documentoDAO.getTodosPorFuncionarioDescricao(usuario.getFuncionario(), consulta);
    }
    
    public String salvar(){
        
        AssentamentoDAO assentamentoDAO = new AssentamentoDAO();
        try{
            encaminhamento.setDocumento(documentoSelecionado);
            encaminhamento.setDepartamentoRemetente(usuario.getFuncionario().getDepartamento());
            encaminhamento.setEncaminhamentoStatus(EnumStatusEncaminhamento.ENCAMINHADO.getStatus());
            
            encaminhamentoDAO.beginTransation();
            
            encaminhamentoDAO.salvar(encaminhamento);
            assentamento.setFuncionario(usuario.getFuncionario());
            assentamento.setEncaminhamento(encaminhamento);
            assentamentoDAO.salvar(assentamento);
            
            encaminhamentoDAO.commitTransation();
            
            Mensagens.msgInfo("O encaminhamento do documento '"
                               +documentoSelecionado.getDocumentoDescricao()+"' para o "
                               +encaminhamento.getDepartamentoDestino().getDepartamentoNome()
                               +" foi realizado com sucesso!");
            zerarObjetos();
            CtrlDocumentos ctrlDocumentos = (CtrlDocumentos) Sessao.recuperarManageBean("#{ctrlDocumentos}", CtrlDocumentos.class);
            ctrlDocumentos.carregaEncaminhamentosRecebidos();
        }
        catch(Exception e){
        	encaminhamentoDAO.rollbackTransation();
        	e.printStackTrace();
            Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente novamente.");
        }
        informarPaginaRedirecionamento();
        return "mensagens";
    }
    
    private void informarPaginaRedirecionamento() {
		Mensagens.informarPaginaRedirecionamento("encaminhamento");
	}
}
