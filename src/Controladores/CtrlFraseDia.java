/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Beans.Frase;
import Util.Frases;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlFraseDia implements Serializable {
	
	private static final long serialVersionUID = 3281387368128325419L;
	private Frase frase;
    
    public CtrlFraseDia() {
        frase = Frases.carregaFrase();
    }

    public Frase getFrase() {
        return frase;
    }

    public void setFrase(Frase frase) {
        this.frase = frase;
    }
    
    
}
