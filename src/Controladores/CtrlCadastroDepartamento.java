/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Beans.Departamento;
import DAO.DepartamentoDAO;
import Util.Mensagens;


/**
 *
 * @author lucas
 */

@ManagedBean
@ViewScoped
public class CtrlCadastroDepartamento implements Serializable {

	private static final long serialVersionUID = 5180893755960540062L;
	private Departamento departamento;
    private DepartamentoDAO departamentoDAO;

    public CtrlCadastroDepartamento() {
        departamento = new Departamento();
        departamentoDAO = new DepartamentoDAO();
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String salvar(){
        try{
        	departamentoDAO.beginTransation();
            departamentoDAO.salvar(departamento);
            departamentoDAO.commitTransation();
            Mensagens.msgInfo("O departamento foi salvo com sucesso!");
            departamento = new Departamento();
        }
        catch(Exception e){
        	departamentoDAO.rollbackTransation();
        	e.printStackTrace();
            Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente Novamente.");

        }
        Mensagens.informarPaginaRedirecionamento("cadastro_departamentos");
        return "mensagens";
    }

}
