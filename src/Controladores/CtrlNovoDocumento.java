package Controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;

import org.primefaces.component.tabview.Tab;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.TabChangeEvent;

import Beans.Documento;
import Beans.Usuario;
import DAO.DocumentoDAO;
import Util.Arquivo;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlNovoDocumento implements Serializable {
	
	private static final long serialVersionUID = 4424503226624547890L;
	private Documento documento;
    private DocumentoDAO documentoDAO;
    private String editor;
    private String documentoNome;
    private String activetab;

    public CtrlNovoDocumento() {
        documento = new Documento();
        documentoDAO = new DocumentoDAO();
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public String getEditor() {
        return editor;
    }

    public void setEditor(String editor) {
        this.editor = editor;
    }

    public String getDocumentoNome() {
        return documentoNome;
    }

    public void setDocumentoNome(String documentoNome) {
        this.documentoNome = documentoNome;
    }
    
    public void onChange(TabChangeEvent event) {
        Tab tab = event.getTab();
        activetab=tab.getId();
    }

    public String getActivetab() {
        return activetab;
    }

    public void setActivetab(String activetab) {
        this.activetab = activetab;
    }
    
    

    public List<SelectItem> getTiposDocumentos() {
        List<SelectItem> tipos = new ArrayList<SelectItem>();
        tipos.add(new SelectItem("Memorando"));
        return tipos;
    }

    public String salvarDocumento() {
        try {
            Usuario u = (Usuario) Sessao.recuperarObjetoSessao("usuario");
            documento.setFuncionario(u.getFuncionario());
            documento.setDepartamento(u.getFuncionario().getDepartamento());
            documento.setDocumentoCaminhoArquivo(Arquivo.moverArquivo(documento.getDocumentoCaminhoArquivo()));
            
            documentoDAO.beginTransation();
            documento = documentoDAO.salvar(documento);
            documentoDAO.commitTransation();
            
            Mensagens.msgInfo("Sucesso, o documento foi enviado para o servidor!");
            limparFormulario();

        } catch (Exception e) {
        	e.printStackTrace();
        	documentoDAO.rollbackTransation();
            Mensagens.msgErro("O documento não pode ser enviado. Tente novamente mais tarde.");
        }
        informarPaginaRedirecionamento();
        return "mensagens";
    }

    public void digitalizarDocumento() {
    }

    public String salvarDocumentoEditor() {

        try {
        	String CAMINHO = Arquivo.criarDocumento(editor, documentoNome + ".pdf");
            Usuario u = (Usuario) Sessao.recuperarObjetoSessao("usuario");
            documento.setFuncionario(u.getFuncionario());
            documento.setDepartamento(u.getFuncionario().getDepartamento());
            documento.setDocumentoCaminhoArquivo(CAMINHO);
            
            documentoDAO.beginTransation();
            documentoDAO.salvar(documento);
            documentoDAO.commitTransation();
            Mensagens.msgInfo("Sucesso, o documento foi salvo no servidor.");
            limparFormulario();
            
        } 
        catch (Exception e) {
        	e.printStackTrace();
        	documentoDAO.rollbackTransation();
            Mensagens.msgErro("Erro, o documento não pode ser salvo. Tente novamente.");
        }
        informarPaginaRedirecionamento();
        return "mensagens" ;
    }

	private void informarPaginaRedirecionamento() {
		Mensagens.informarPaginaRedirecionamento("novo_documento");
	}

	private void limparFormulario() {
		documento = new Documento();
		editor="";
		documentoNome="";
	}

    public void handleFileUpload(FileUploadEvent event) {
    	String caminhoArquivo = Arquivo.uploadArquivo(event);
    	if(caminhoArquivo == null) {
    		Mensagens.msgErro("Erro ao tentar realizar o upload do arquivo.");
    	}
    	else{
    		documento.setDocumentoCaminhoArquivo(caminhoArquivo);
    		Mensagens.msgInfo("Arquivo enviado com sucesso.");
    	}
    }
}
