/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Beans.Cargo;
import Beans.Departamento;
import DAO.CargoDAO;
import DAO.DepartamentoDAO;
import Util.Mensagens;


/**
 *
 * @author lucas
 */

@ManagedBean
@ViewScoped
public class CtrlCadastroCargo implements Serializable {
	
	private static final long serialVersionUID = 6158669109292368810L;
	private Cargo cargo;
    private CargoDAO cargoDAO;

    public CtrlCadastroCargo() {

        cargo = new Cargo();
        cargoDAO = new CargoDAO();

    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }

    public List<Departamento> getDepartamentos(){
        DepartamentoDAO departamentoDAO = new DepartamentoDAO();
        return departamentoDAO.todos();

    }

    public String salvar(){
        try{
        	cargoDAO.beginTransation();
            cargoDAO.salvar(cargo);
            cargoDAO.commitTransation();
            Mensagens.msgInfo("O cargo foi salvo com sucesso!");
            cargo = new Cargo();
        }
        catch(Exception e){
        	cargoDAO.rollbackTransation();
        	e.printStackTrace();
            Mensagens.msgInfo("Não foi possível realizar a sua solicitação. Tente novamente.");
        }
        Mensagens.informarPaginaRedirecionamento("cadastro_cargos");
        return "mensagens";
    }

}
