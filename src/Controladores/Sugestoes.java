/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;
import java.util.Date;

import Beans.Sugestao;
import DAO.SugestaoDAO;


/**
 *
 * @author lucas
 */
public class Sugestoes implements Serializable {

	private static final long serialVersionUID = -6143430160168424258L;
	private Sugestao sugestao;
    private SugestaoDAO dao;
   
    public Sugestoes() {
        sugestao = new Sugestao();
        dao = new SugestaoDAO();
    }

    public Sugestao getSugestao() {
        return sugestao;
    }

    public void setSugestao(Sugestao sugestao) {
        this.sugestao = sugestao;
    }

    public void salvar(){

        try{
            sugestao.setSugestaoData(new Date());
            dao.beginTransation();
            dao.salvar(sugestao);
            dao.commitTransation();
            sugestao = new Sugestao();
        }
        catch(Exception e){
            e.printStackTrace();
            dao.rollbackTransation();
        }

    }

}
