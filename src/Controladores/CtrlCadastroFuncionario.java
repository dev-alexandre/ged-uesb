/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Util.Mensagens;
import Beans.Cargo;
import Beans.Departamento;
import Beans.Funcionario;
import DAO.CargoDAO;
import DAO.DepartamentoDAO;
import DAO.FuncionarioDAO;


/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlCadastroFuncionario implements Serializable {

	private static final long serialVersionUID = 2313151917052003341L;
	private Funcionario funcionario;
    private FuncionarioDAO dao;
    private List<Cargo> cargos;

    public CtrlCadastroFuncionario() {
        funcionario = new Funcionario();
        dao = new FuncionarioDAO();
        cargos = new ArrayList<Cargo>();
    }

    public Funcionario getFuncionario() {
        return funcionario;
    }

    public void setFuncionario(Funcionario funcionario) {
        this.funcionario = funcionario;
    }

    public List<Departamento> getDepartamentos(){
        DepartamentoDAO departamentoDAO = new DepartamentoDAO();
        return departamentoDAO.todos();
    }

    public List<Cargo> getCargos() {
        return cargos;
    }

    public void setCargos(List<Cargo> cargos) {
        this.cargos = cargos;
    }    

    public void carregaCargos(){
        CargoDAO cargoDAO = new CargoDAO();
        if (funcionario.getDepartamento() == null) {
        	cargos.clear();        
        	
        }
        else {
        	cargos = cargoDAO.buscarCargoPorDepartamento(funcionario.getDepartamento().getDepartamentoId());        
        }

    }

    public String salvar(){

        try{
        	dao.beginTransation();
            dao.salvar(funcionario);
            dao.commitTransation();
            Mensagens.msgInfo("Funcionário salvo com sucesso.");
            funcionario = new Funcionario();
        }
        catch(Exception e){
        	dao.rollbackTransation();
        	e.printStackTrace();
        	Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente novamente.");
        	
        }
        Mensagens.informarPaginaRedirecionamento("cadastro_funcionarios");
        return "mensagens";
    }

}
