/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.primefaces.component.tabview.Tab;
import org.primefaces.event.TabChangeEvent;

import Beans.Documento;
import Beans.Encaminhamento;
import Beans.Usuario;
import DAO.DocumentoDAO;
import DAO.EncaminhamentoDAO;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlDocumentos implements Serializable {

	private static final long serialVersionUID = 4543874935419555416L;
	private List<Documento> documentos;
    private List<Encaminhamento> encaminhamentosFeitos;
    private List<Encaminhamento> encaminhamentosRecebidos;
    private Documento documentoSelecionado;
    private Encaminhamento encaminhamentoSelecionado;
    private String consulta;
    private int activetab;
    
    private Usuario usuario;
    private DocumentoDAO documentoDAO;
    private EncaminhamentoDAO encaminhamentoDAO;
            
    public CtrlDocumentos() {
        documentoDAO = new DocumentoDAO();
        encaminhamentoDAO = new EncaminhamentoDAO();
    }
    
    @PostConstruct
    public void load() {
    	recuperaSessao();
    	carregaEncaminhamentosRecebidos();
    	carregaDocumentosDoFuncionario();
    	carregaEncaminhamentosFeitos();    	
    }

    public Documento getDocumentoSelecionado() {
        return documentoSelecionado;
    }

    public void setDocumentoSelecionado(Documento documentoSelecionado) {
        this.documentoSelecionado = documentoSelecionado;
    }

    public int getActivetab() {
        return activetab;
    }

    public void setActivetab(int activetab) {
        this.activetab = activetab;
    }
    
    public void onChange(TabChangeEvent event) {
	Tab tab = event.getTab();
        activetab = (Integer.parseInt(tab.getId().substring(3, 4))-1);
    }


    public String getConsulta() {
        return consulta;
    }

    public void setConsulta(String consulta) {
        this.consulta = consulta;
    }
    
    public List<Documento> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(List<Documento> documentos) {
        this.documentos = documentos;
    }
    
    public void carregaDocumentosDoFuncionario(){
        documentos=documentoDAO.getTodosPorFuncionario(usuario.getFuncionario());
    }
    
    public void carregaDocumentosDoFuncionarioPorDescricao(){
        documentos=documentoDAO.getTodosPorFuncionarioDescricao(usuario.getFuncionario(), consulta);
    }

    public List<Encaminhamento> getEncaminhamentosFeitos() {
        return encaminhamentosFeitos;
    }

    public void setEncaminhamentosFeitos(List<Encaminhamento> encaminhamentosFeitos) {
        this.encaminhamentosFeitos = encaminhamentosFeitos;
    }

    public List<Encaminhamento> getEncaminhamentosRecebidos() {
        return encaminhamentosRecebidos;
    }

    public void setEncaminhamentosRecebidos(List<Encaminhamento> encaminhamentosRecebidos) {
        this.encaminhamentosRecebidos = encaminhamentosRecebidos;
    }

    public Encaminhamento getEncaminhamentoSelecionado() {
        return encaminhamentoSelecionado;
    }

    public void setEncaminhamentoSelecionado(Encaminhamento encaminhamentoSelecionado) {
        this.encaminhamentoSelecionado = encaminhamentoSelecionado;
    }

    public void carregaEncaminhamentosRecebidos(){
        encaminhamentosRecebidos=encaminhamentoDAO.encaminhamentosRecebidosPorDepartamento(usuario.getFuncionario().getDepartamento());
    }
    
    public void carregaEncaminhamentosRecebidosPorDocumentoDescricao(){
        encaminhamentosRecebidos=encaminhamentoDAO.encaminhamentosRecebidosPorDepartamentoDocumentoDescricao(usuario.getFuncionario().getDepartamento(), consulta);
    }
    
    public void carregaEncaminhamentosFeitos(){
        encaminhamentosFeitos=encaminhamentoDAO.encaminhamentosFeitosPorFuncionario(usuario.getFuncionario());
    }
    
    public void carregaEncaminhamentosFeitosPorDocumentoDescricao(){
        encaminhamentosFeitos=encaminhamentoDAO.encaminhamentosFeitosPorFuncionarioDescricao(usuario.getFuncionario(),consulta);
    }
    
    private void recuperaSessao() {
        usuario = (Usuario) Sessao.recuperarObjetoSessao("usuario");
        
        Encaminhamento encaminhamento = (Encaminhamento) Sessao.recuperarRemoverObjetoSessao("encaminhamentoSelecionado");
        if(encaminhamento!=null) {
        	encaminhamentoSelecionado = encaminhamento;        	
        }
    }
    
    public String visualizarDocumentoEncaminhado() {
        try {
        	Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
        	return "visualizar_documento_encaminhado.jsf?faces-redirect=true";
        } 
        catch (Exception ex) {
        }
        return null;
    }
    
    public String visualizarDocumentoRecebido() {
        try {
        	Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
        	return "visualizar_documento_recebido.jsf?faces-redirect=true";
        } 
        catch (Exception ex) {
        }
        return null;
    }
    
    public String visualizarDocumento() {
        try {
        	Sessao.adicionarObjetoSessao("documentoSelecionado", documentoSelecionado);
        	return "visualizar_documento.jsf?faces-redirect=true";
        } 
        catch (Exception ex) {
        }
        return null;
    }
    
    public String excluir(){
        
        try{
        	documentoDAO.beginTransation();
            documentoDAO.excluir(documentoSelecionado);
            documentoDAO.commitTransation();
            Mensagens.msgInfo("O documento '"+documentoSelecionado.getDocumentoDescricao()+"' foi excluído com sucesso!");
            documentoSelecionado = new Documento();
        }
        catch(Exception e){
        	e.printStackTrace();
        	documentoDAO.rollbackTransation();
            Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente novamente.");
        }
        informarPaginaRedirecionamento();
        return "mensagens";
    }
    
    
    public String salvar(){
        try{
        	documentoDAO.beginTransation();
        	if(documentoSelecionado.getDocumentoId()==null) {
        		documentoDAO.salvar(documentoSelecionado);
        	}
        	else {
        		documentoDAO.atualizar(documentoSelecionado);
        	}
            documentoDAO.commitTransation();
            Mensagens.msgInfo("O documento '"+documentoSelecionado.getDocumentoDescricao()+"' foi altualizado com sucesso!");
            documentoSelecionado = new Documento();            
        }
        catch(Exception e){
        	e.printStackTrace();
        	documentoDAO.rollbackTransation();
            Mensagens.msgErro("Não foi possível realizar a sua solicitação. Tente novamente.");
            
        }
        informarPaginaRedirecionamento();
        return "mensagens";
    }
    
    private void informarPaginaRedirecionamento() {
		Mensagens.informarPaginaRedirecionamento("documentos");
	}
    
    
}