/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Beans.Usuario;
import DAO.UsuarioDAO;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@SessionScoped
public class CtrlLogin implements Serializable {

	private static final long serialVersionUID = -5203696723323315669L;
	private UsuarioDAO dao;
    private Usuario usuario;
    
    public void msg() {
    	String msg = (String) Sessao.recuperarRemoverObjetoSessao("msg");
    	if(msg!=null) {
    		Mensagens.msgAlerta(msg);
    	}
    }
    
    public String login() {
        Usuario u = dao.validaUsuario(usuario);
        if (u != null) {
            Sessao.adicionarObjetoSessao("usuario", u);
            usuario=u;
            return "home.jsf?faces-redirect=true";
            
        } else {
            usuario=new Usuario();
            Mensagens.msgAlerta("Usuario ou senha inexistente.");
            return "";
        }

    }
    
    public void logout() {        
        if(Sessao.recuperarObjetoSessao("usuario")!=null) {
            Sessao.removerObjetoSessao("usuario");
            usuario = new Usuario();
        }        
    }

    public CtrlLogin() {
        dao = new UsuarioDAO();
        usuario = new Usuario();
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
