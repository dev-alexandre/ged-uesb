/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@SessionScoped
public class CtrlMensagens implements Serializable {
    
	private static final long serialVersionUID = 3873340682131631674L;
	private String pagina;
    private String activetab;

    public CtrlMensagens() {
    }

    public String getPagina() {
        return pagina;
    }

    public void setPagina(String pagina) {
        this.pagina = pagina;
    }

    public String getActivetab() {
        return activetab;
    }

    public void setActivetab(String activetab) {
        this.activetab = activetab;
    }

    public String confirmacao() {
        
    	String redirecionamento = null;
    	if(pagina != null) {
	        if(pagina.equals("documentos")){
	            CtrlDocumentos ctrlDocumentos = (CtrlDocumentos) Sessao.recuperarManageBean("#{ctrlDocumentos}", CtrlDocumentos.class);
	            ctrlDocumentos.carregaDocumentosDoFuncionario();
	        }
	        redirecionamento = new String(pagina);
	        pagina = null;
        }
        
    	return redirecionamento;
    }
}
