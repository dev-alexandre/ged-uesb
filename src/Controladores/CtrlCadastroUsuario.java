/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package Controladores;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import Beans.Funcionario;
import Beans.Usuario;
import DAO.FuncionarioDAO;
import DAO.UsuarioDAO;


/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlCadastroUsuario implements Serializable {
	
	private static final long serialVersionUID = -1400071094361072166L;
	private Usuario usuario;
    private UsuarioDAO usuarioDAO;
    private List<Funcionario> funcionarios;
    private String strConsulta;

    public CtrlCadastroUsuario() {

        usuario = new Usuario();
        usuario.setFuncionario(new Funcionario());
        usuarioDAO = new UsuarioDAO();
        strConsulta = "";
        carregaFuncionarios();
        

    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getStrConsulta() {
        return strConsulta;
    }

    public void setStrConsulta(String strConsulta) {
        this.strConsulta = strConsulta;
    }

    public List<Funcionario> getFuncionarios() {
        return funcionarios;
    }

    public void setFuncionarios(List<Funcionario> funcionarios) {
        this.funcionarios = funcionarios;
    }

    

    public void Salvar(){

        try{
        	usuarioDAO.beginTransation();
            usuarioDAO.salvar(usuario);
            usuarioDAO.commitTransation();
            usuario = new Usuario();
        }
        catch(Exception e){
        	e.printStackTrace();
        	usuarioDAO.rollbackTransation();
        }
    }
    
    public void carregaFuncionarios(){
       
        FuncionarioDAO funcionarioDAO = new FuncionarioDAO();
        
        try{
            String sql = "select funcionario "
                    +"from Funcionario funcionario "
                    +"where upper(funcionario.funcionarioNome) like :funcionario";

            Map<String, Object> params = new HashMap<String, Object>();

            params.put("funcionario", strConsulta.toUpperCase() + "%");
            funcionarios=funcionarioDAO.listPesqParam(sql, params);
            
        }
        catch(Exception e){
            
        }        
         
    }

}
