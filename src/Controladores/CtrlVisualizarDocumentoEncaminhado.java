/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controladores;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.StreamedContent;

import Beans.Assentamento;
import Beans.Encaminhamento;
import Beans.Usuario;
import DAO.AssentamentoDAO;
import DAO.EncaminhamentoDAO;
import Util.Arquivo;
import Util.EnumStatusEncaminhamento;
import Util.Mensagens;
import Util.Sessao;

/**
 *
 * @author lucas
 */
@ManagedBean
@ViewScoped
public class CtrlVisualizarDocumentoEncaminhado implements Serializable {

	private static final long serialVersionUID = 4543874935419555416L;
    
	private boolean renderedTextArea;
	private String textoAssentamento;
	private Encaminhamento encaminhamentoSelecionado;
	private Usuario usuario;
	            
    public CtrlVisualizarDocumentoEncaminhado() {
    }
    
    @PostConstruct
    public void load() {
    	recuperaSessao();
    	renderedTextArea = false;
    }
    
    private void recuperaSessao() {
    	usuario = (Usuario) Sessao.recuperarObjetoSessao("usuario");
    	encaminhamentoSelecionado = (Encaminhamento) Sessao.recuperarRemoverObjetoSessao("encaminhamentoSelecionado");
    }

	public void redirectToHome() {
		if(encaminhamentoSelecionado == null) {
    		try {
    			FacesContext.getCurrentInstance().getExternalContext().redirect("home.jsf");
			} 
    		catch (IOException e) {
				e.printStackTrace();
			}
    	}
	}
    
    public StreamedContent getDownloadArquivo() {
    	try{
    		String caminhoArquivo = encaminhamentoSelecionado.getDocumento().getDocumentoCaminhoArquivo();
    		return Arquivo.downloadArquivo(caminhoArquivo);
    	}
    	catch(Exception e) {
    		return null;
    	}
    }
    
    public String encerrarTramitacao() {
    	encaminhamentoSelecionado.setEncaminhamentoStatus(EnumStatusEncaminhamento.FINALIZADO.getStatus());
    	EncaminhamentoDAO encaminhamentoDAO = new EncaminhamentoDAO();
    	try{
    		encaminhamentoDAO.beginTransation();
    		encaminhamentoDAO.atualizar(encaminhamentoSelecionado);    				
    		encaminhamentoDAO.commitTransation();
    		Mensagens.msgInfo("Tramitação encerrada.");
    		Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		encaminhamentoDAO.rollbackTransation();    		
    		Mensagens.msgErro("Não foi possível realizar a operação solicitada.");
    	}
    	informarPaginaRedirecionamento();
    	return "mensagens";
    	
    }
    
    public String enviarAssentamento() {
    	Assentamento assentamento = new Assentamento();
    	assentamento.setAssentamentoConteudo(textoAssentamento);
    	assentamento.setEncaminhamento(encaminhamentoSelecionado);
    	assentamento.setFuncionario(usuario.getFuncionario());
    	
    	AssentamentoDAO assentamentoDAO = new AssentamentoDAO();
    	try{
    		encaminhamentoSelecionado.getAssentamentoList().add(assentamento);
    		assentamentoDAO.beginTransation();
    		assentamentoDAO.salvar(assentamento);
    		assentamentoDAO.commitTransation();
    		Mensagens.msgInfo("Sua mensagem foi enviada com sucesso.");
    		Sessao.adicionarObjetoSessao("encaminhamentoSelecionado", encaminhamentoSelecionado);
    	}
    	catch(Exception e) {
    		e.printStackTrace();
    		assentamentoDAO.rollbackTransation();
    		Mensagens.msgErro("Não foi possível realizar a operação solicitada.");
    	}
    	informarPaginaRedirecionamento();
    	return "mensagens";
    	
    }
    
    private void informarPaginaRedirecionamento() {
		Mensagens.informarPaginaRedirecionamento("visualizar_documento_encaminhado");
	}

    public Encaminhamento getEncaminhamentoSelecionado() {
    	return encaminhamentoSelecionado;
    }
    
    public void setEncaminhamentoSelecionado(Encaminhamento encaminhamentoSelecionado) {
    	this.encaminhamentoSelecionado = encaminhamentoSelecionado;
    }
    
	public boolean isRenderedTextArea() {
		return renderedTextArea;
	}

	public void setRenderedTextArea(boolean renderedTextArea) {
		this.renderedTextArea = renderedTextArea;
	}

	public String getTextoAssentamento() {
		return textoAssentamento;
	}

	public void setTextoAssentamento(String textoAssentamento) {
		this.textoAssentamento = textoAssentamento;
	}
    
}