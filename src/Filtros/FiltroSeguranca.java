/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Filtros;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Beans.Usuario;

/**
 *
 * @author lucas
 */
public class FiltroSeguranca implements Filter {
    
	private final String ROOT_PAGE = "/index.jsf";
	private final String HOME_PAGE = "/home.jsf";
	private final String LOGOUT_PAGE = "/logout";
    
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        
    	HttpServletResponse httpServletResponse = ((HttpServletResponse) response);
    	HttpServletRequest httpServletRequest = ((HttpServletRequest) request);
    	HttpSession sessao = httpServletRequest.getSession();
        
        Usuario u = (Usuario) sessao.getAttribute("usuario");
        
        String contextPath = httpServletRequest.getContextPath();
        String currentPage = httpServletRequest.getServletPath();
        
        if(currentPage.equals(contextPath)) {
        	chain.doFilter(request, response);
        }
        else if(currentPage.equals(LOGOUT_PAGE)) {
        	sessao.removeAttribute("usuario");
        	httpServletResponse.sendRedirect(contextPath+ROOT_PAGE);
        }
        else if(currentPage.equals(ROOT_PAGE)) {
        	if(validUser(u)){
        		httpServletResponse.sendRedirect(contextPath+HOME_PAGE);
        	}
        	chain.doFilter(request, response);
        }
        else if(validUser(u) || canAccess(currentPage)) {
            chain.doFilter(request, response);
        }
        else{
            sessao.setAttribute("msg", "Você ainda não está logado no sistema.");            
            httpServletResponse.sendRedirect(contextPath+ROOT_PAGE);
        }
    }

	private boolean validUser(Usuario u) {
		return u!=null;
	}
    
    private boolean canAccess(String url) {
    	if(url.contains("/javax.faces.resource/") || url.contains("/images/")){
    		return true;
    	}
		return freePages().contains(url);
    }

	private List<String> freePages() {
		return Arrays.asList(new String [] {
	    		"/style.css",
	    		"/layout.xhtml"
	    });
	}

	@Override
	public void destroy() {
		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		
	}
    

}
