/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

import Controladores.CtrlMensagens;

/**
 *
 * @author lucas
 */
public class Mensagens {
	
	public static void informarPaginaRedirecionamento(String pagina) {
		CtrlMensagens ctrlMensagens = (CtrlMensagens) Sessao.recuperarManageBean("#{ctrlMensagens}", CtrlMensagens.class);
		ctrlMensagens.setPagina(pagina);
	}
   
    
    public static void msgErro(String msg){
        
        msg(msg,FacesMessage.SEVERITY_ERROR);
        
    }
    
    public static void msgAlerta(String msg){
        
        msg(msg,FacesMessage.SEVERITY_WARN);
        
    }
    
    public static void msgInfo(String msg){
        
        msg(msg,FacesMessage.SEVERITY_INFO);
        
    }
    
    public static void msg(String msg, FacesMessage.Severity severity){
        
        FacesContext.getCurrentInstance().addMessage("msg", new FacesMessage(severity, msg, null));
        
    }
    
}
