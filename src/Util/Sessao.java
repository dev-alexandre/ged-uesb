/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

/**
 *
 * @author lucas
 */
public class Sessao {
    
    public static Object recuperarObjetoSessao(String nome){
        HttpSession session = getSession();
        return session.getAttribute(nome);
        
    }
    
    public static Object recuperarRemoverObjetoSessao(String nome){
        HttpSession session = getSession();
        Object obj = session.getAttribute(nome);
        session.removeAttribute(nome);
        return obj;
    }
    
    public static void adicionarObjetoSessao(String nome, Object obj){
        HttpSession session = getSession();
        session.setAttribute(nome, obj);
        
    }
    
    public static void removerObjetoSessao(String nome){
        HttpSession session = getSession();
        session.removeAttribute(nome);
    }
    
    public static Object recuperarManageBean(String managebean, Class<?> classe){
        
        FacesContext fCtx = FacesContext.getCurrentInstance();
        ELContext elCtx = fCtx.getELContext();
        ExpressionFactory ef = fCtx.getApplication().getExpressionFactory();
        
        ValueExpression ve = ef.createValueExpression(elCtx,managebean, classe);
        
        return ve.getValue(elCtx);
    }
    
    private static HttpSession getSession() {
    	FacesContext fc = FacesContext.getCurrentInstance();
    	HttpSession session = (HttpSession) fc.getExternalContext().getSession(false);
    	return session;
    }
}
