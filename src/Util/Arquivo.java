/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

import javax.activation.MimetypesFileTypeMap;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.lowagie.text.DocumentException;

/**
 *
 * @author lucas
 */
public class Arquivo {

	private static final String ARQUIVO_DIR = "/var/ged-uesb/arquivos";
	private static final String ARQUIVO_TMP_DIR = "/tmp/ged-uesb/arquivos";

	public static StreamedContent downloadArquivo(String caminhoArquivo) throws FileNotFoundException {
		InputStream stream = new FileInputStream(caminhoArquivo);
		String nomeArquivo = caminhoArquivo.substring(caminhoArquivo.lastIndexOf('/')+1, caminhoArquivo.length());
		String contentType = new MimetypesFileTypeMap().getContentType(nomeArquivo);
		return new DefaultStreamedContent(stream, contentType, nomeArquivo);
	}

	public static String uploadArquivo(FileUploadEvent event) {
		UploadedFile uploadedFile = event.getFile();
		String diretorio = ARQUIVO_TMP_DIR + "/" + new Date();
		String caminhoArquivo = diretorio + "/" + uploadedFile.getFileName();
		File file = new File(diretorio);

		try {
			file.mkdirs();
			
			file = new File(caminhoArquivo); 

			InputStream bufferIn = uploadedFile.getInputstream();
			FileOutputStream bufferOut = new FileOutputStream(file);

			while (bufferIn.available() != 0) {
				bufferOut.write(bufferIn.read());
			}

			bufferOut.close();
			bufferIn.close();
			file = null;
			return caminhoArquivo;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static String criarDocumento(String conteudo, String documento) throws DocumentException, IOException {
		String caminho = ARQUIVO_DIR + new Date() + documento; 
		OutputStream out = new FileOutputStream(caminho);
        PDF.converterHtmlPdf(conteudo, out);
        out.close();
        return caminho;
	}

	public static String moverArquivo(String caminhoArquivo) {
		File file = new File(caminhoArquivo);
		String novoCaminhoArquivo = ARQUIVO_DIR + caminhoArquivo.replace(ARQUIVO_TMP_DIR, "");
		String novoDiretorio = novoCaminhoArquivo.substring(0,caminhoArquivo.lastIndexOf('/'));
		
		File newFile = new File(novoDiretorio);
		newFile.mkdirs();
		
		file.renameTo(newFile = new File(novoCaminhoArquivo));
		file = null;
		newFile = null;
		
		return novoCaminhoArquivo;
	}
}
