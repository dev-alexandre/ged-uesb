/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Image;
import com.lowagie.text.DocumentException;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import org.w3c.tidy.Tidy;
import org.xhtmlrenderer.pdf.ITextRenderer;

/**
 *
 * @author lucas
 */
public class PDF {
    
    public static void converterHtmlPdf(String html, OutputStream out) throws DocumentException {
        
        //definindo a codificação dos caracteres
        Tidy tidy = new Tidy();
        tidy.setInputEncoding("utf-8");
        tidy.setOutputEncoding("utf-8");
        
        //buffer quer recebe o texto digitado na pagina
        //html
        byte[] buffer = html.getBytes();
        ByteArrayInputStream input = new ByteArrayInputStream(buffer);
        
        //conversão de um documento html em documento de
        //texto puro, mantendo as formatações
        org.w3c.dom.Document doc = tidy.parseDOM(input, null);
        
        //renderização e geração do documento digital
        //no formato pdf
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, null);
        renderer.layout();
        renderer.createPDF(out);

    }
    
    public static void converterImagemPdf(BufferedImage bufferedImage, OutputStream outputStream) throws com.itextpdf.text.DocumentException, BadElementException, IOException{
        
        Document documento = new com.itextpdf.text.Document();
        PdfWriter writer =  PdfWriter.getInstance(documento, outputStream);
        
        documento.open();
        
        Image img = Image.getInstance(writer,bufferedImage,1.0f);
        documento.add(img);
        
        documento.close();
        writer.close();
        
    }
    
}
