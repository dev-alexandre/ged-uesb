package Util;

public enum EnumStatusEncaminhamento {
	
	ENCAMINHADO("Encaminhado"),
	RECEBIDO("Recebido"),
	FINALIZADO("Finalizado")	
	;
	
	private String status;
	
	private EnumStatusEncaminhamento(String status) {
		this.status = status;
	}
	
	public String getStatus() {
		return status;		
	}

}
