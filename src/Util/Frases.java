/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import Beans.Frase;
import DAO.FraseDAO;
import java.util.List;

/**
 *
 * @author lucas
 */
public class Frases {
    
    public static Frase carregaFrase(){
        
        FraseDAO fraseDAO = new FraseDAO();
        
        List<Frase> frases = fraseDAO.todos();
        
        int sorteio = (int) (1 + Math.random() * frases.size());
                
        return frases.get((sorteio-1));
        
    }
    
}
