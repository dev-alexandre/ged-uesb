/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

import Beans.Departamento;
import DAO.DepartamentoDAO;

/**
 *
 * @author lucas
 */

@FacesConverter("ConversorDepartamento")
public class ConversorDepartamento implements Converter {

	private DepartamentoDAO dao = new DepartamentoDAO();
	private Departamento departamento = null;
	private Integer id;

	public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
		if (string == null || string.equals("Selecione...")) {
			return null;
		}
		else{
			id = Integer.parseInt(string);
			departamento = dao.pesquisarPorId(id);
			return departamento;			
		}
	}

	public String getAsString(FacesContext fc, UIComponent uic, Object o) {
		if(o == null) {
			return null;
		}
		else{
			departamento = (Departamento) o;
			return String.valueOf(departamento.getDepartamentoId());
		}
	}
}
