/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Conversores;

import Beans.Cargo;
import DAO.CargoDAO;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

/**
 *
 * @author lucas
 */

@FacesConverter("ConversorCargo")
public class ConversorCargo implements Converter {

    private CargoDAO dao = new CargoDAO();
    private Cargo cargo = null;
    private Integer id;

    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
    	if(string == null || string.equals("Selecione...")) {
    		return null;
    	}
    	else{
    		id = Integer.parseInt(string);
    		cargo=dao.pesquisarPorId(id);
    		return cargo;
    	}
    }

    public String getAsString(FacesContext fc, UIComponent uic, Object o) {        
        cargo=(Cargo)o;
        return String.valueOf(cargo.getCargoId());
    }
}
